const express = require('express');
const routes = require("./routes");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/', (req, res) => {
    res.status(200).json({ status: 200, success: true, message: 'Welcome to my API' });
});

app.use('/api', routes);

app.listen(5000, () => {
    console.log(`Server is running on port http://localhost:5000`);
});