const express = require('express');
const UserRouter = require("./user.route");
const ProductRouter = require("./product.route");

const router = express.Router();

router.use('/users', UserRouter);
router.use('/products', ProductRouter);

module.exports = router;