const express = require('express');
const ProductController = require("../controllers/product.controller");

const router = express.Router();

router.get('/', ProductController.index);
router.get('/:id', ProductController.show);
router.post('/', ProductController.create);
router.patch('/:id', ProductController.update);
router.delete('/:id', ProductController.delete);

module.exports = router;