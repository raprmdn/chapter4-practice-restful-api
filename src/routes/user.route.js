const express = require('express');
const UserController = require("../controllers/user.controller");

const router = express.Router();

router.get('/', UserController.index);
router.get('/:id', UserController.show);
router.post('/', UserController.create);
router.patch('/:id', UserController.update);
router.delete('/:id', UserController.delete);

module.exports = router;