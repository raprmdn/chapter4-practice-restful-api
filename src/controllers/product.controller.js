const data = require('../data.json');
const fs = require('fs');

module.exports = {
    index: (req, res) => {
        try {
            const products = data.products;
            res.status(200).json({
                status: 200,
                success: true,
                message: 'Success get all products',
                data: products
            });
        } catch (err) {
            res.status(500).json({ status: 500, success: false, message: err.message });
        }
    },
    show: (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const product = data.products.find(product => product.id === id);
            if (!product) return res.status(404).json({ status: 404, success: false, message: 'Product not found' });

            res.status(200).json({
                status: 200,
                success: true,
                message: 'Success get product',
                data: product
            });
        } catch (err) {
            res.status(500).json({ status: 500, success: false, message: err.message });
        }
    },
    create: (req, res) => {
        try {
            const { name, description, price } = req.body;
            const product = { id: data.products.length + 1, name, description, price };
            data.products.push(product);
            fs.writeFileSync('./src/data.json', JSON.stringify(data, null, 2));

            res.status(201).json({
                status: 201,
                success: true,
                message: 'Success create product',
                data: product
            });
        } catch (err) {
            res.status(500).json({ status: 500, success: false, message: err.message });
        }
    },
    update: (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const product = data.products.find(product => product.id === id);
            if (!product) return res.status(404).json({ status: 404, success: false, message: 'Product not found' });

            product.name = req.body.name;
            product.description = req.body.description;
            product.price = req.body.price;
            fs.writeFileSync('./src/data.json', JSON.stringify(data, null, 2));

            res.status(200).json({
                status: 200,
                success: true,
                message: 'Success update product',
            });
        } catch (err) {
            res.status(500).json({ status: 500, success: false, message: err.message });
        }
    },
    delete: (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const product = data.products.find(product => product.id === id);
            if (!product) return res.status(404).json({ status: 404, success: false, message: 'Product not found' });

            const index = data.products.indexOf(product);
            data.products.splice(index, 1);
            fs.writeFileSync('./src/data.json', JSON.stringify(data, null, 2));

            res.status(200).json({
                status: 200,
                success: true,
                message: 'Success delete product',
            });
        } catch (err) {
            res.status(500).json({ status: 500, success: false, message: err.message });
        }
    }
}