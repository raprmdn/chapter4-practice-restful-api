const data = require('../data.json');
const fs = require('fs');

module.exports = {
    index: (req, res) => {
        try {
            const users = data.users;
            res.status(200).json({
                status: 200,
                success: true,
                message: 'Success get all users',
                data: users
            });
        } catch (err) {
            res.status(500).json({ status: 500, success: false, message: err.message });
        }
    },
    show: (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const user = data.users.find(user => user.id === id);
            if (!user) return res.status(404).json({ status: 404, success: false, message: 'User not found' });

            res.status(200).json({
                status: 200,
                success: true,
                message: 'Success get user',
                data: user
            });
        } catch (err) {
            res.status(500).json({ status: 500, success: false, message: err.message });
        }
    },
    create: (req, res) => {
        try {
            const { name, email } = req.body;
            const user = { id: data.users.length + 1, name, email };
            data.users.push(user);
            fs.writeFileSync('./src/data.json', JSON.stringify(data, null, 2));

            res.status(201).json({
                status: 201,
                success: true,
                message: 'Success create user',
                data: user
            });
        } catch (err) {
            res.status(500).json({ status: 500, success: false, message: err.message });
        }
    },
    update: (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const user = data.users.find(user => user.id === id);
            if (!user) return res.status(404).json({ status: 404, success: false, message: 'User not found' });

            user.name = req.body.name;
            user.email = req.body.email;
            fs.writeFileSync('./src/data.json', JSON.stringify(data, null, 2));

            res.status(200).json({
                status: 200,
                success: true,
                message: 'Success update user',
            });
        } catch (err) {
            res.status(500).json({ status: 500, success: false, message: err.message });
        }
    },
    delete: (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const user = data.users.find(user => user.id === id);
            if (!user) return res.status(404).json({ status: 404, success: false, message: 'User not found' });

            const index = data.users.indexOf(user);
            data.users.splice(index, 1);
            fs.writeFileSync('./src/data.json', JSON.stringify(data, null, 2));

            res.status(200).json({
                status: 200,
                success: true,
                message: 'Success delete user',
            });
        } catch (err) {
            res.status(500).json({ status: 500, success: false, message: err.message });
        }
    }
}